package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

import java.util.ArrayList;
import java.util.List;

@Component
public class MovieListMapper implements Converter<List<MovieDto>,List<Movie>> {
    public MovieListMapper() {
    }

    @Override
    public List<MovieDto> convert(List<Movie> from) {
        List<MovieDto> moviesDto = new ArrayList<>();
        for(Movie movie: from) {
            MovieDto movieDto = new MovieDto.Builder()
                    .image(movie.getImage())
                    .movieId(movie.getMovieId())
                    .title(movie.getTitle())
                    .year(movie.getYear())
                    .build();
            moviesDto.add(movieDto);
        }
        return moviesDto;
    }
}
