package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.DetailsMovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

@Component
public class MovieDetailsMapper implements Converter<DetailsMovieDto, Movie> {
    public MovieDetailsMapper() {
    }
    @Override
    public DetailsMovieDto convert(Movie from) {

        return new DetailsMovieDto.Builder()
                .title(from.getTitle())
                .image(from.getImage())
                .videoId(from.getVideoId())
                .year(from.getYear())
                .build();
    }
}
