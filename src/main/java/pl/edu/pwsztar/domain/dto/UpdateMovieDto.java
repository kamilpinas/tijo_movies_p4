package pl.edu.pwsztar.domain.dto;

import java.io.Serializable;

public class UpdateMovieDto implements Serializable {
    private String title;
    private String videoId;
    private String image;
    private Integer year;

    private UpdateMovieDto(Builder builder){
        title = builder.title;
        videoId=builder.videoId;
        image=builder.image;
        year=builder.year;
    }
    public UpdateMovieDto(){}
    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public Integer getYear() {
        return year;
    }

    public String getVideoId() {
        return videoId;
    }

    public static final class Builder{
        private String title;
        private String videoId;
        private String image;
        private Integer year;
        public Builder(){}

        public Builder title(String title){
            this.title=title;
            return this;
        }
        public Builder image(String image){
            this.image=image;
            return this;
        }
        public Builder videoId(String videoId){
            this.videoId=videoId;
            return this;
        }
        public Builder year(Integer year){
            this.year=year;
            return this;
        }
        public UpdateMovieDto build(){
            return new UpdateMovieDto(this);
        }
    }
}
